;;;; package.lisp

(defpackage #:cl-config
  (:use #:cl)
  (:export #:*defaults-config-file*
           #:*overrides-config-file*
           #:load-config
           #:conf))

