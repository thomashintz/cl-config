(in-package #:cl-config)

(defparameter *settings* (make-hash-table))

(defparameter *defaults-config-file* "")
(defparameter *overrides-config-file* "")

(defun read-config-file (file)
  (when (probe-file file)
    (with-open-file (f file)
      (dolist (setting (read f))
        (setf (gethash (car setting) *settings*) (cadr setting))))))

(defun load-config ()
  (read-config-file *defaults-config-file*)
  (read-config-file *overrides-config-file*))

(defun conf (setting)
  (gethash setting *settings*))
